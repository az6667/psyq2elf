CFLAGS := -g -Os -std=c99 -Wall -Wextra

PSYQ2ELF := psyq2elf
PSYQDUMP := psyqdump

PSYQ2ELF_SRC := \
	src/elf_obj.c \
	src/psyq2elf.c \
	src/psyq_obj.c \
	src/utils.c

PSYQDUMP_SRC := \
	src/psyqdump.c

PSYQ2ELF_OBJ := $(PSYQ2ELF_SRC:.c=.o)
PSYQDUMP_OBJ := $(PSYQDUMP_SRC:.c=.o)

all: $(PSYQ2ELF) $(PSYQDUMP)

$(PSYQ2ELF): $(PSYQ2ELF_OBJ)
	$(CC) -o $@ $^

$(PSYQDUMP): $(PSYQDUMP_OBJ)
	$(CC) -o $@ $^

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	rm -f $(PSYQ2ELF) $(PSYQDUMP) $(PSYQ2ELF_OBJ) $(PSYQDUMP_OBJ)

.PHONY: all clean
