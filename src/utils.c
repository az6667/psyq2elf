/* See LICENSE file for copyright and license details. */

#include <assert.h>
#include <stdlib.h>

#include "utils.h"

void *
xcalloc(size_t nmemb, size_t size)
{
	void *p;

	p = calloc(nmemb, size);
	assert(p != NULL);

	return p;
}

void *
xrealloc(void *ptr, size_t size)
{
	void *p;

	p = realloc(ptr, size);
	assert(p != NULL);

	return p;
}
