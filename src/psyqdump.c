/* See LICENSE file for copyright and license details. */

#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

#define PSYQ_LIB_SIGNATURE	"LIB"
#define PSYQ_MODULE_NAME_LENGTH	8

static const char *argv0;

static char buf[BUFSIZ];

static char *
vstrcat(const char *str, ...)
{
	va_list ap;
	const char *s;
	char *res;
	size_t len = 0;

	va_start(ap, str);

	for (s = str; s != NULL; s = va_arg(ap, const char *))
		len += strlen(s);

	va_end(ap);

	if ((res = calloc(1, len + 1)) == NULL)
		return NULL;

	va_start(ap, str);

	for (s = str; s != NULL; s = va_arg(ap, const char *))
		strcat(res, s);

	va_end(ap);

	return res;
}

static int
dump_module(FILE *file, const char *dir)
{
	uint8_t name[PSYQ_MODULE_NAME_LENGTH + 1] = { 0 };
	FILE *f;
	char *path;
	uint32_t date;
	uint32_t link_offset;
	uint32_t next_offset;
	size_t size;
	long pos;
	int status;
	int i;

	pos = ftell(file);

	if (fread(&name, PSYQ_MODULE_NAME_LENGTH, 1, file) != 1)
		return 1;

	for (i = PSYQ_MODULE_NAME_LENGTH - 1; i > 0; --i) {
		if (isspace(name[i]))
			name[i] = '\0';
		else
			break;
	}

	path = vstrcat(dir, "/", name, ".OBJ", NULL);
	assert(path != NULL);

	READ_FIELD(file, date);
	READ_FIELD(file, link_offset);
	READ_FIELD(file, next_offset);

	next_offset += pos;
	pos += link_offset;

	assert(fseek(file, pos, SEEK_SET) != -1);

	f = fopen(path, "wb");
	assert(f != NULL);

	free(path);

	while (!feof(file) && !ferror(file) && !feof(f) && !ferror(f)) {
		if (pos >= next_offset)
			break;
		else if (pos + sizeof(buf) > next_offset)
			size = next_offset - pos;
		else
			size = sizeof(buf);

		assert(fread(buf, size, 1, file) == 1);
		assert(fwrite(buf, size, 1, f) == 1);

		pos += size;
	}

	status = 0;

	if (ferror(file) || ferror(f))
		status = -1;

	if (fclose(f))
		status = -1;

	return status;
}

static int
dump_lib(FILE *file, const char *dir)
{
	uint8_t signature[3];
	uint8_t version;
	int status = 0;

	READ_FIELD(file, signature);
	assert(!memcmp(signature, PSYQ_LIB_SIGNATURE,
		       strlen(PSYQ_LIB_SIGNATURE)));

	READ_FIELD(file, version);
	assert(version == 1);

	while (!status && !feof(file) && !ferror(file)) {
		status = dump_module(file, dir);
		if (status < 0)
			return status;
		else if (status > 0)
			break;
	}

	return ferror(file);
}

static int
psyqdump(const char *path, const char *dir)
{
	FILE *file;

	if ((file = fopen(path, "rb")) == NULL)
	{
		fprintf(stderr, "Failed to open %s.\n", path);
		return 1;
	}

	if (dump_lib(file, dir))
	{
		fprintf(stderr, "Failed to dump lib to %s.\n", dir);
		return 1;
	}

	if (fclose(file))
	{
		fprintf(stderr, "Failed to close %s.\n", path);
		return 1;
	}

	return 0;
}

static void
usage(void)
{
	fprintf(stderr, "usage: %s <library> <directory>\n", argv0);
}

int
main(int argc, char *argv[])
{
	argv0 = argv[0];

	if (argc != 3) {
		usage();
		return 1;
	}

	return psyqdump(argv[1], argv[2]);
}
